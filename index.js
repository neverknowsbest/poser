const path = require('path'),
	fs = require('fs'),
	emotes = require('./emotes')

module.exports = function Poser(mod) {
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Initiate variables
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	let idling = true,
		spawnid = 33333333333333,
		meincombat = 0,
		refreshspawns = null,
		emoteLoop = null,
		emotetimer = 0,
		fslock = [],
		lastemote = {emote: 14, rate: 1},
		ploc,
		selfcostumes = {},
		chardb = {},
		cache = {},
		selfmount = 0,
		summlist = {}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	function FilterSlots(types, slots) {
		if (slots == null) return []
		slots = slots.split(",")
		types = types.split(",")
		let res = []
		for (let s in summlist) {
			if (types.includes(summlist[s].type) && (slots.includes("all") || slots.includes(s) || slots.includes(summlist[s].id+''))) res.push(s)
		}
		return res
	}
	
	function Spawn_ID() {
		let spawnids = [], spawns = Object.keys(summlist)
		for (let i = 0; i < spawns.length; i++) { spawnids.push(summlist[spawns[i]].id) }
		if (spawnids.length > 0) {
			spawnids.sort()
			for (let m = 0; m < spawnids.length; m++) { 
				if (m == 0 && spawnids[0] > 0) return 0
				if (m > 0 && spawnids[m] - spawnids[m-1] > 1) return spawnids[m-1] + 1
			}
			return spawnids[spawnids.length - 1] + 1
		}
		return 0
	}
		
	function Customize(l) { 
		let c = {}, resl = 0n
		//customize
		if (l && l.skinColor != null) return l
		//stringified bigint
		else if (l && typeof l === 'string') resl = BigInt(l)
		//long
		else if (l && l.low != null && l.high != null && l.unsigned != null) {
			resl = BigInt(l.low >>> 0) | BigInt(l.unsigned ? l.high >>> 0 : l.high | 0) << 32n
		}
		Object.assign(c, {
			unk:		Number(resl & 0xffn),
			skinColor:	Number(resl >> 8n & 0xffn),
			faceStyle:	Number(resl >> 16n & 0xffn),
			faceDecal:	Number(resl >> 24n & 0xffn),
			hairStyle:	Number(resl >> 32n & 0xffn),
			hairColor:	Number(resl >> 40n & 0xffn),
			voice:		Number(resl >> 48n & 0xffn),
			tattoos:	Number(resl >> 56n & 0xffn)
		});
		return c
	}

	function parseBuffer(b) {
		if (b && b.data) {
			return Buffer.from(b.data)
		} else if (b && typeof b === 'string') {
			return Buffer.from(b, 'hex')
		}
		return 0
	}
	
	function LoadName(p) {
		if (GetData(p)) {
			return true
		} else if (fslock.includes(p)) {
			return false
		} else {
			fslock.push(p)
			mod.command.message('Loading ' +p)
			fs.readFile(path.join(__dirname, 'characters', p+'.json'), function(err,data) {
				if (!err && data) {
					mod.command.message(p+' loaded')
					cache = JSON.parse(data)
				} else {
					mod.command.message(p+' does not exist')
				}
				fslock.splice(fslock.indexOf(p), 1)
			})
			return false
		}
	}
	
	function SaveName(p, item, msg = false) {
		if (fslock.includes(p)) return
		fslock.push(p)
		fs.mkdir(path.join(__dirname, 'characters'), { recursive: false }, (err) => {
			if (err && err.code != 'EEXIST') {
				mod.command.message('Failed to create characters folder')
				return
			}
		})
		fs.writeFile(path.join(__dirname, 'characters', p+'.json'), JSON.stringify(item, null, '\t'), err => {
			if (err) mod.command.message('Failed to save data to ' +p+'.json')
			else if (msg) mod.command.message('Saved data of ' +p)
			fslock.splice(fslock.indexOf(p), 1)
		})
	}
	
	function GetData(p) {
		if (chardb[p]) return chardb[p]
		if (summlist[p] && summlist[p].type == 'player') return summlist[p].data
		if (cache.name && cache.name == p) return cache
		return null
	}
	
	function SetData(slots) {
		let spawns = FilterSlots("player", slots)
		for (let i = 0; i < spawns.length; i++) {
			if (chardb[spawns[i]]) Object.assign(chardb[spawns[i]], summlist[spawns[i]].data)
			SaveName(spawns[i], summlist[spawns[i]].data, true)
		}
	}
		
	function SetCostume(slots, costume, id, target) {
		let spawns = FilterSlots("player", slots),
			tspawns = FilterSlots("player", target),
			slotname = '',
			res = 0,
			refresh = false
		let source = (id == 'copy' && tspawns.length > 0 ? summlist[tspawns[0]].data : selfcostumes)
		switch (costume) {
		case 'armor': slotname = 'body'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'helmet': slotname = 'head'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'mask': slotname = 'face'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'gloves': slotname = 'hand'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'boots': slotname = 'feet'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'gear': slotname = 'weapon'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'body': slotname = 'styleBody'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'head': slotname = 'styleHead'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'face': slotname = 'styleFace'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'weapon': slotname = 'styleWeapon'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'back':  slotname = 'styleBack'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'undies': slotname = 'underwear'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'footstep': slotname = 'styleFootprint'; res = (id == 'copy' ? source[slotname] : parseInt(id, 10)); break
		case 'showface': slotname = 'showFace'; res = (id ? true : false); break
		case 'tag': slotname = 'styleTag'; res = id; break
		case 'dye': 
			if (id == 'copy') { 
				slotname = 'styleBodyDye'; res = source.styleBodyDye
			} else {
				id = id.split(",")
				if (id.length === 4) {
					slotname = 'styleBodyDye';
					let z_hex = Math.min(Math.max(Number(id[3]),1),255).toString(16)
					let r_hex = Math.min(Math.max(Number(id[0]),16),255).toString(16)
					let g_hex = Math.min(Math.max(Number(id[1]),16),255).toString(16)
					let b_hex = Math.min(Math.max(Number(id[2]),16),255).toString(16)
					res = Number('0x'+z_hex+r_hex+g_hex+b_hex)
				}
			}
			break
		}
		for (let i = 0; i < spawns.length; i++) {
			if (slotname) {
				summlist[spawns[i]].data[slotname] = res
				refresh = true
			}
		}
		if (refresh) SpawnPlayers()
	}
			
	function SpawnPlayers(){
		let extract = {}, spawns = FilterSlots("player", "all")
		for (let i = 0; i < spawns.length; i++) {
			if (summlist[spawns[i]].summ) {
				mod.send('S_DESPAWN_USER', 3, { gameId: spawnid + summlist[spawns[i]].id, type: 1})
				if (summlist[spawns[i]].summ == 2) {
					delete summlist[spawns[i]]; continue
				}
			}
			extract = summlist[spawns[i]].data
			let e = Object.assign({}, extract, {
				appearance: Customize(extract.appearance),
				serverId: mod.game.me.serverId, playerId: mod.game.me.playerId, gameId: spawnid + summlist[spawns[i]].id, loc: summlist[spawns[i]].sloc.loc, w: summlist[spawns[i]].sloc.w,
				relation: 1, visible: 1, alive: 1, pose: summlist[spawns[i]].sloc.type, mount: (summlist[spawns[i]].mount > 0 ? summlist[spawns[i]].mount : 0), 
				showStyle: 1, appearance2: 100,
				name: '['+summlist[spawns[i]].id+']'+spawns[i],
				shape: parseBuffer(extract.shape),
				details: parseBuffer(extract.details)
			})
			delete e.styleTag
			mod.send('S_SPAWN_USER', 16, e)
			if (extract.styleTag) {
				mod.send('S_ITEM_CUSTOM_STRING', 2, { gameId: spawnid + summlist[spawns[i]].id, customStrings: [{dbid: extract.styleBody, string: extract.styleTag}]})
			}
			summlist[spawns[i]].summ = 1
		}
		mod.clearInterval(emoteLoop)
		mod.clearTimeout(refreshspawns)
		if (spawns.length) {
			refreshspawns = mod.setTimeout(SpawnPlayers, 120000)
			ApplySummShape('all', 'reapply', 4)
			EmotePlayers('all', 'last', null)
		}
		if (emotetimer > 0) mod.setTimeout(() => { emoteLoop = mod.setInterval(EmotePlayers, emotetimer, 'all,me', 'loop', null) }, 200) 
	}
	
	function SpawnMounts(slots, id){
		let spawns = FilterSlots("player", slots)
		for (let i = 0; i < spawns.length; i++) {
			if (summlist[spawns[i]].sloc.type != 7) continue
			if (id != summlist[spawns[i]].mount) {
				mod.send('S_UNMOUNT_VEHICLE', 2, {
					gameId: spawnid + summlist[spawns[i]].id, skill: 1170030000
				})
			}
			if (id) {
				summlist[spawns[i]].mount = id
				mod.send('S_MOUNT_VEHICLE', 2, {
					gameId: spawnid + summlist[spawns[i]].id, id: id, skill: 1170030000, unk: 0
				})
			} else {
				summlist[spawns[i]].mount = 0
			}
		}
	}
	
	function ReposSpawns(slots, flags, target){
		let refresh = false, spawns = FilterSlots("player,sign,item", slots),
			tspawns = FilterSlots("player,sign,item", target)
		if (!ploc) return
		for (let i = 0; i < spawns.length; i++) {
			if (tspawns.length > 0 && tspawns[0] === i+'') continue
			let source = (tspawns.length > 0 ? summlist[tspawns[0]].sloc : ploc)
			switch (flags) {
			case 'look':
				if (summlist[spawns[i]].type != "player") break
				summlist[spawns[i]].sloc.w = (Math.atan2(summlist[spawns[i]].sloc.loc.y - source.loc.y, 
					summlist[spawns[i]].sloc.loc.x - source.loc.x) + 2 * Math.PI) % (2 * Math.PI) - Math.PI
				mod.send('S_USER_LOCATION', 5, {
                    gameId: spawnid + summlist[spawns[i]].id,
                    loc: summlist[spawns[i]].sloc.loc,
                    w: summlist[spawns[i]].sloc.w,
                    unk: 0,
                    speed: 1,
                    dest: summlist[spawns[i]].sloc.loc,
                    type: summlist[spawns[i]].sloc.type
				})
				break
			case 'run':
			case 'walk':
				if (summlist[spawns[i]].type != "player") break
				summlist[spawns[i]].emote = 'nothing'
				mod.send('S_USER_LOCATION', 6, {
					gameId: spawnid + summlist[spawns[i]].id,
					loc: summlist[spawns[i]].sloc.loc,
					w: summlist[spawns[i]].sloc.w,
					speed: 1,
					dest: source.loc,
					type: (summlist[spawns[i]].sloc.type == 8 ? 8 : (flags == 'run' ? 0 : 1))
				})
				break
			default: 
				switch (summlist[spawns[i]].type) {
				case 'player': summlist[spawns[i]].sloc = source; refresh = true; break
				case 'sign': 
				case 'item': CreateProp(spawns[i], 'repos'); break
				}
			}
		}
		if (refresh) SpawnPlayers()
	}
	
	function ApplySummShape(slots, shape, stack){
		let spawns = FilterSlots("player", slots)
		for (let i = 0; i < spawns.length; i++) {
			if (summlist[spawns[i]].changer[shape]) summlist[spawns[i]].changer[shape].stacks = stack
			for (let c in summlist[spawns[i]].changer) {
				if (shape == 'reapply' && summlist[spawns[i]].changer[c].stacks == 4) continue
				mod.send('S_ABNORMALITY_BEGIN', 4, {
					target: spawnid + summlist[spawns[i]].id,
					source: spawnid + summlist[spawns[i]].id,
					id: summlist[spawns[i]].changer[c].id,
					duration: 0,
					unk: 0,
					stacks: summlist[spawns[i]].changer[c].stacks,
					unk2: 0,
					unk3: 0
				})
			}
		}
	}

	function CreateProp(prop, operation) {
		if (!summlist[prop]) return
		switch (summlist[prop].type) {
		case 'item':
			mod.send('S_DESPAWN_DROPITEM', 4, { gameId: spawnid + summlist[prop].id });
			if (operation == 'delete') { 
				delete summlist[prop]
			} else {
				if (operation == 'repos') summlist[prop].sloc = ploc
				mod.send('S_SPAWN_DROPITEM', 9, {
					gameId: spawnid + summlist[prop].id,
					loc: summlist[prop].sloc.loc,
					item: summlist[prop].item,
					amount: 1,
					expiry: 600000,
					source: spawnid + summlist[prop].id,
					owners: [{ playerId: mod.game.me.playerId }],
					ownerName : '['+summlist[prop].id+']'+summlist[prop].ownerName,
				})
			}
		break
		case 'sign':
			mod.send('S_DESPAWN_BUILD_OBJECT', 2, { gameId : spawnid + summlist[prop].id, unk : 0 });
			if (operation == 'delete') { 
				delete summlist[prop]
			} else {
				if (operation == 'repos') summlist[prop].sloc = ploc
				mod.send('S_SPAWN_BUILD_OBJECT', 2, {
					gameId : spawnid + summlist[prop].id,
					itemId : summlist[prop].itemid,
					loc : summlist[prop].sloc.loc,
					w : summlist[prop].sloc.w,
					ownerName : '['+summlist[prop].id+']'+summlist[prop].ownerName,
					message : summlist[prop].message
				})
			}
		break
		}
	}
	
	function DespawnPlayers(slots) {
		let spawns = FilterSlots("player,sign,item", slots), refresh = false
		for (let i = 0; i < spawns.length; i++) {
			switch (summlist[spawns[i]].type) {
			case 'player': summlist[spawns[i]].summ = 2; refresh = true; break
			case 'sign': 
			case 'item': CreateProp(spawns[i], 'delete'); break
		}}
		if (refresh) {
			clearTimeout(refreshspawns)
			refreshspawns = mod.setTimeout(SpawnPlayers, Object.keys(summlist).length * 5)
		}
	}
	
	function ChangeSpawnsClass(slots, job) {
		let spawns = FilterSlots("player", slots)
		for (let i = 0; i < spawns.length; i++) {
			let tId = Math.floor(summlist[spawns[i]].data.templateId / 100),
			jobid = 0
			switch (job) {
			case 'lancer': jobid = 2; break;
			case 'slayer': jobid = 3; break;
			case 'berserker': jobid = 4; break;
			case 'sorcerer': jobid = 5; break;
			case 'archer': jobid = 6; break;
			case 'priest': jobid = 7; break;
			case 'mystic': jobid = 8; break;
			case 'reaper': jobid = ([110].includes(tId) ? 9 : 1); break;
			case 'gunner': jobid = ([104,108,110].includes(tId) ? 10 : 4); break;
			case 'brawler': jobid = ([101,102,109,110].includes(tId) ? 11 : 2); break;
			case 'ninja': jobid = ([110].includes(tId) ? 12 : 5); break;
			case 'valkyrie': jobid = ([108].includes(tId) ? 13 : 3); break;
			default: jobid = 1;
			}
			summlist[spawns[i]].data.templateId = (tId * 100) + jobid
		}
		SpawnPlayers()
	}

	function EmotePlayers(slots, emote, rate) {
		let myself = slots.split(","), spawns = FilterSlots("player", slots)
		if (myself.includes("me")) {
			if (!['loop', 'last', 'repeat'].includes(emote)) {
				lastemote = findEmote(mod.game.me.templateId, emote, lastemote.rate)
				if (['outweapon', 'inweapon'].includes(lastemote.emote)) {
					meincombat = (meincombat ? 0 : 1); 
					lastemote.emote = (meincombat ? 'outweapon' : 'inweapon')
				}
			}
			if (rate) lastemote.rate = Math.min(Math.max(parseFloat(rate, 10), 0.1), 20)
			idling = false
			StartEmote('me', lastemote, 0)
		}
		for (let i = 0; i < spawns.length; i++) {
			if (!['loop', 'last', 'repeat'].includes(emote)) {
				summlist[spawns[i]].emote = findEmote(summlist[spawns[i]].data.templateId, emote, summlist[spawns[i]].emote.rate)
				if (['outweapon', 'inweapon'].includes(summlist[spawns[i]].emote.emote)) {
					summlist[spawns[i]].combat = (summlist[spawns[i]].combat ? 0 : 1);
					summlist[spawns[i]].emote.emote = (summlist[spawns[i]].combat ? 'outweapon' : 'inweapon')
				}
			}
			if (rate) summlist[spawns[i]].emote.rate = Math.min(Math.max(parseFloat(rate, 10), 0.1), 20)
			StartEmote(spawns[i], summlist[spawns[i]].emote, (emote == 'last' ? 2 : 0))
		}
    }
	
	function StartEmote(who, anims, refresh) {
		// Odd workaround for some animations cannot be played on other players
		if (summlist[who] && refresh === 2) {
			mod.send('S_SOCIAL', 1, { target: spawnid + summlist[who].id, animation: 14 });
			mod.setTimeout(StartEmote, 200, who, anims, 1)
			return
		}
		switch (typeof anims.emote) {
		case "number":
			mod.send('S_SOCIAL', 1, {
				target: (who === 'me' ? mod.game.me.gameId : spawnid + summlist[who].id), 
				animation: anims.emote
			});
		break;
		case "string":
			mod.send('S_PLAY_ANIMATION', 1, {
				gameId: (who === 'me' ? mod.game.me.gameId : spawnid + summlist[who].id), 
				rate: anims.rate,
				animName: anims.emote
			})
			// Set Combat Status
			if (['outweapon', 'inweapon'].includes(anims.emote) || refresh === 1) {
				if (who === 'me') {
					mod.send('S_USER_STATUS', 3, {
						gameId: mod.game.me.gameId, 
						status: meincombat,
						bySkill: (meincombat ? true : false)
					});
				} else if (summlist[who]) {
					mod.send('S_USER_WEAPON_APPEARANCE_CHANGE', 2, {
						gameId: spawnid + summlist[who].id,
						weapon: (summlist[who].data.styleWeapon || summlist[who].data.weapon),
						unk1: 0,
						weaponEnchant: summlist[who].data.weaponEnchant,
						styleWeapon: summlist[who].data.styleWeapon,
						unk2: true,
						abnormalityEffect: (summlist[who].combat ? 329 : 0)
					});
				}		
			}
		break;
		}
	}

	
	function findEmote(tid, e, speed) {
		let job = (tid % 100)
		let rg = Math.floor(tid / 100)
		if (/^\d+$/.test(e)) return { emote: parseInt(e, 10), rate: speed }
		for (let i in emotes) {
			if (i === e || emotes[i].name.includes(e)) {
				if (emotes[i].otherEmote) {
					if (emotes[i].otherEmote[`${job}`]) {
						return { 
							emote: emotes[i].otherEmote[`${job}`].emote, 
							rate: (emotes[i].otherEmote[`${job}`].rate ? emotes[i].otherEmote[`${job}`].rate : speed)
						}
					} else if (emotes[i].otherEmote[`${rg}`]) {
						return { 
							emote: emotes[i].otherEmote[`${rg}`].emote, 
							rate: (emotes[i].otherEmote[`${rg}`].rate ? emotes[i].otherEmote[`${rg}`].rate : speed),
						}
					}
				}
				return { 
					emote: emotes[i].emote, 
					rate: (emotes[i].rate ? emotes[i].rate : speed)
				}
			}
		}
		return { emote: e, rate: speed }
	}
	
	function CaptureUserData(datatype, event) {
		let { 
            weapon, body, hand, feet, underwear, head, face, weaponModel, bodyModel, handModel, feetModel, 
			styleHead, styleFace, styleWeapon, styleBack, styleBody, styleFootprint, 
			styleHeadScale, styleHeadRotation, styleHeadTranslation, styleHeadTranslationDebug, 
			styleFaceScale, styleFaceRotation, styleFaceTranslation, styleFaceTranslationDebug,
            styleBackScale, styleBackRotation, styleBackTranslation, styleBackTranslationDebug, accessoryTransformUnk, styleBodyDye, showStyle 
		} = event
		let extract = { 
            weapon, body, hand, feet, underwear, head, face, weaponModel, bodyModel, handModel, feetModel, 
			styleHead, styleFace, styleWeapon, styleBack, styleBody, styleFootprint, 
			styleHeadScale, styleHeadRotation, styleHeadTranslation, styleHeadTranslationDebug, 
			styleFaceScale, styleFaceRotation, styleFaceTranslation, styleFaceTranslationDebug,
            styleBackScale, styleBackRotation, styleBackTranslation, styleBackTranslationDebug, accessoryTransformUnk, styleBodyDye, showStyle 
		}
		if (datatype != 'costumes') {
			Object.assign(extract, {
				name: event.name,
				appearance: event.appearance,
				details: event.details.toString('hex'),
				shape: event.shape.toString('hex'),
				templateId: (datatype == 'self' ? 10101 + parseInt(event.class) + parseInt(event.gender * 100) + parseInt(event.race * 200) : event.templateId),
				styleTag: (event.styleTag ? event.styleTag : '')
			})
		}
		return extract
	}

	function CancelPoser() {
		idling = true
		cache = {}
		selfmount = 0
		emotetimer = 0
		meincombat = 0
		mod.clearInterval(emoteLoop)
		DespawnPlayers('all')
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//mod.commands
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	mod.command.add('-spawn', (who, item, item2, item3) => {
		switch(who) {
		case 'loot': 
			let iid = Spawn_ID()
			summlist['item_'+iid] = Object.assign({}, {
				type: 'item',
				id: iid,
				item: (item ? parseInt(item, 10) : 369),
				ownerName: (item2 ? item2 : ''),
				sloc: ploc
			})
			CreateProp('item_'+iid, "create")
		break
		case 'sign':
			let signid = Spawn_ID()
			summlist['sign_'+signid] = Object.assign({}, { 
				type: 'sign',
				id: signid,
				itemid: (item2 ? Math.min(Math.max(parseInt(item2, 10), 1), 3) : 1),
				message: (item ? item : 'Poser'),
				ownerName: (item3 ? item3 : ''),
				sloc: ploc
			})
			CreateProp('sign_'+signid, "create")
		break
		default:
			if (who && LoadName(who)) {
				if (summlist[who] != null) return
				let data = GetData(who), sid = Spawn_ID()
				summlist[who] = Object.assign({}, {
					type: 'player',
					data: data, 
					summ: 0, 
					id: sid, 
					sloc: ploc, 
					combat: 0,
					mount: 0,
					emote: {emote: 14, rate: 1},
					changer: {
						size: {id: 7000005, stacks:4}, 
						thighs: {id: 7000014, stacks:4}, 
						height: {id: 7000013, stacks:4}, 
						chest: {id: 7000012, stacks:4} 
					}
				})
				SpawnPlayers()
				mod.command.message('Player ' +who+ ' spawned');
			}
		}
	})

	mod.command.add('-respawn', () => {
		SpawnPlayers()
	})

	mod.command.add('-dress', (who, which, what, whom) => {
		if (what != null) SetCostume(who, which, what, whom)
	})
	
	mod.command.add('-save', (who) => {
		if (who != null) SetData(who)
	})

	mod.command.add('-shape', (who, shape, stack) => {
		if (stack != null) ApplySummShape(who, shape, parseInt(stack, 10))
	})

	mod.command.add('-ride', (who, mountid) => {
		if (who != null) SpawnMounts(who, (mountid != null ? mountid : selfmount))
	})
	
	mod.command.add('-sign', (which, message, itemid, owner) => {
		let pid = parseInt(which, 10)
		if (message && summlist['sign_'+pid] && summlist['sign_'+pid].type == 'sign') {}
		else return
		summlist['sign_'+pid].itemid = (itemid ? Math.min(Math.max(parseInt(itemid, 10), 1), 3) : summlist['sign_'+pid].itemid)
		summlist['sign_'+pid].message = message
		summlist['sign_'+pid].ownerName = (owner ? owner : summlist['sign_'+pid].ownerName)
		CreateProp('sign_'+pid, "create")
	});

	mod.command.add('-loot', (which, item, owner) => {
		let pid = parseInt(which, 10)
		if (item && summlist['item_'+pid] && summlist['item_'+pid].type == 'item') {}
		else return
		summlist['item_'+pid].item = (item ? parseInt(item, 10) : summlist['item_'+pid].itemid)
		summlist['item_'+pid].ownerName = (owner ? owner : summlist['item_'+pid].ownerName)
		CreateProp('item_'+pid, "create")
	});
	
	mod.command.add('-repos', (who, lookat, whom) => {
		if (who != null) ReposSpawns(who, lookat, whom)
	})
	
	mod.command.add('-emote', (who, emote, speed) => {
		switch (who) {
			case 'loop': 
				if (emote != null) emotetimer = parseInt(emote, 10)
				mod.clearInterval(emoteLoop)
				EmotePlayers('all,me', 'repeat', null)
				if (emotetimer > 0) emoteLoop = mod.setInterval(EmotePlayers, emotetimer, 'all,me', 'loop', null)
				break;
			default: 
				if (emote != null && emote != 'last') EmotePlayers(who, emote, speed)
				else EmotePlayers('all,me', 'repeat', null);
		}
	})
	
	mod.command.add('-job', (who, job) => {
		if (job != null) ChangeSpawnsClass(who, job)
	})


	mod.command.add('-despawn', (who) => {
		if (who != null) DespawnPlayers(who)
	})

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Hooks
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	mod.hook('S_GET_USER_LIST', 18, { order: 10000 }, (event) => {
        for (let index in event.characters) {
			let ex = CaptureUserData('self', event.characters[index])
			chardb[event.characters[index].name] = ex
			SaveName(event.characters[index].name, ex)
        }
    })
	
	mod.hook('S_USER_EXTERNAL_CHANGE', 7, { order: 1000000, filter: {fake: null} }, (event) => {
		if(mod.game.me.is(event.gameId)) {
			Object.assign(selfcostumes, event)
			if (chardb[mod.game.me.name]) {
				let ex = CaptureUserData('costumes', event)
				Object.assign(chardb[mod.game.me.name], ex)
			}
		}
	})
	
	mod.hook('S_UNICAST_TRANSFORM_DATA', 6, { order: 1000000, filter: { fake: null }}, event => {
		if(mod.game.me.is(event.gameId)) {
			Object.assign(selfcostumes, event)
			if (chardb[mod.game.me.name]) {
				let ex = CaptureUserData('costumes', event)
				Object.assign(chardb[mod.game.me.name], ex)
			}
		}
 	})
	
	mod.game.on('leave_game', () => {
		CancelPoser()
	})

	mod.hook('S_MOUNT_VEHICLE', 2, { order: 5 }, (event) => {
		if(mod.game.me.is(event.gameId)) selfmount = event.id
	})
	
	mod.hook('S_UNMOUNT_VEHICLE', 2, { order: 5 }, (event) => {
		if(mod.game.me.is(event.gameId)) selfmount = 0
	})
	
	mod.hook('C_PLAYER_LOCATION', 5, event =>{ 
		ploc = event; idling = (event.type == 7)
		if (!idling) {
			if (emoteLoop) {
				clearInterval(emoteLoop)
				emotetimer = 0
			}
		}
	})
	
	mod.hook('S_PLAYER_STAT_UPDATE', 14, (e) => {if(meincombat){e.status = 1; return true}})
	mod.hook('S_USER_STATUS', 3, (e) => {if(meincombat){e.status = 1; return true}})
	
    mod.hook('S_SOCIAL', 1, (event) => {
        if (mod.game.me.is(event.target) && !idling) {
			return false
		}
    })
	
	mod.game.me.on('change_zone', (zone, quick) => {
		CancelPoser()
	})
	
}
