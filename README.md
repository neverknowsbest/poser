# Poser

A mod to allow you to spawn and customize your alts, drop items(you cannot pick them up), and signs. Purely for roleplay purposes.
You can also perform some animations not possible with in-game emotes.
All operations of this module is clientsided. Changes to the spawned characters and props are temporary until
you teleport or leave the game.

Unicast is compatible with Poser; Your spawns can also copy your costumes and mounts from those modules.

## Commands

`-spawn [name]`
Spawn your beloved alt in the place you're standing. Only one unique alt can be presented.
The number after their name is their spawn id. (Enable 'show other player names' in Option).

`-emote [all/id,id,id.../me] [emote/repeat] [speed]`
Makes all spawn player(s) and/or you perform animations from S_SOCIAL(if `[emote]` is valid number) or S_PLAY_ANIMATION(if `[emote]` is valid names in emotes.json).
`[repeat]` , or simply have the command `-emote` repeats the same emote they/you used.
`[speed]` is optional; play the animation speed in float (Default: 1.0).
If `[emote]` is `combat`, triggers combat status. If `combat` status is toggled on yourself, cast certain skills or mount and dismount to take effect.
Spawned players will play fund animation first, as a workaround for some animations unable to play for them.

`-emote loop [milliseconds]`
Makes all spawn player(s) and you repeat the last emote in `[milliseconds]`.

`-ride [all/id,id...] [mountid]`
Make spawn player(s) ride specific `[mountid]`. If `[mountid]` omitted, copies the one you're riding, 
or dismounts if you're not riding anything.

`-dress [all/id,id,id...] [head/face/weapon/body/back/undies/showface/tag/dye] [something/copy] [target/me]`
dress spawned player(s)'s cosmetics to [something]. If `[copy]` is used, copy the specific cosmetic from `[target/me]`.
`[dye]`: `[something]` is r,g,b,a value. eg: `dress 0,1 dye 128,0,48,100`
`[tag]`: `[something]` must be quoted. eg: `-dress 0 tag "Best Loli"`

`-save [all/id,id,id...]`
Temporarily save the spawned player(s)'s cosmetics. The characters' cosmetics will be reset upon re-enter the characters menu.

`-shape [all/id,id,id...] [size/thighs/height/chest] [stacks]`
Morph specific parts of spawned player(s).

`-repos [all/id,id,id...] [lookat/moveto/runto/anything] [target/me]`
`[anything]` or omitted: reposition spawn(s) to another target `[target/me]`.
`[look]`: make spawn player(s) facing another target id/you.
`[move]` and `[run]`: makes the spawn player(s) walk/run to the target slowly.

`-respawn`
Refresh all of the spawned players.
Note that all players automatically respawns for each 2 minutes.

`-job [all/id,id...] [classname]`
Change spawn player(s) class to `[classname]`.

`-spawn sign [message] [type] [owner]`
Spawn a sign to where you're standing. `[message]` must be quoted. 
`[type]` is optional; 1(default) = quarter day, 2 = half day, 3 = full day
`[owner]` is optional; Add the owner's name on top of the sign.

`-sign [id] [message] [type] [owner]`
Change properties of spawned sign `[id]`.

`-spawn loot [itemid] [owner]`
spawn a loot `[itemid]` to where you're standing. 
`[owner]` is optional; Add the owner's name of that loot.

`-loot [id] [itemid] [owner]`
Change properties of spawned loot `[id]`.

`-despawn [all/id,id...]`
Despawn player(s)/loot(s)/sign(s).

## Emote.json example for developers:
```
"recall": { 	//the name of this emote
	"name":  ["allelujah", "god_help_me"],	//alternate names of this emote
	"emote": "Recall",	//actual name of this animation/S_SOCIAL ID number
	"rate": 1,	//Optional; Override the default speed of this emote
	"otherEmote": {	// Optional; Overrides the class/race-gender's emote played
		"1"	{	//warrior
			"emote": "Death",
			"rate": 0.5
		}
	}
}

// IDs used for otherEmote
	//1 warrior
	//2 lancer	101 male human
	//3	slayer	102 female human
	//4	berserker	103 male highelf
	//5	sorcerer	104 female highelf
	//6 archer	105 male aman
	//7 priest	106 female aman
	//8 mystic	107 male castanic
	//9 reaper	108 female castanic
	//10 gunner	109 popori
	//11 brawler	110 elin
	//12 ninja	111 baraka
	//13 valkyrie
```